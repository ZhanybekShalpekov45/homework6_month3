from aiogram import executor
from aiogram.dispatcher.filters import Text
from db.queries import init_db,create_tables,populate_tables
from config import dp


async def bot_start(_):
    init_db()
    create_tables()
    populate_tables()


if __name__ == '__main__':
    executor.start_polling(
        dp,
        on_startup=bot_start,
        skip_updates=True
    )
