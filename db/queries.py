import sqlite3
from pathlib import Path

global db, cursor


def init_db():
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH/DB_NAME)
    cursor = db.cursor()


def create_tables():
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS cars_info_save(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            description TEXT,
            price_usd INTEGER,
            title TEXT,
            url TEXT
        )
        """)
    db.commit()

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS cars_results(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            description TEXT,
            price_usd INTEGER,
            title TEXT,
            url TEXT
        )
    """)
    db.commit()


def populate_tables():
    cursor.execute("""
        INSERT INTO cars_info_save(title, price_usd, description)
        VALUES  ('BMW', '20_000', 'Хэтчбек, серый, 1.8л., левый руль, гибрид, вариатор, передний'),
                ('Mercedes', '30_000', 'Кроссовер, белый, 3л., левый руль, дизель, автомат, постоянный полный')
    """)
    db.commit()


def cars(data):
    cursor.execute(
        """
        INSERT INTO cars_info_save(title, price_usd, description)
        VALUES (:title, :price_usd, :description)
        """,
        {'title': data['title'],
        'price_usd': data['price_usd'],
        'description': data['description']}
    )
    db.commit()
